package com.zuitt.s01d2;

public class DataTypesAndVariables {

    public static void main(String[] args){
        // Variable declaration with initialization
        int firstNumber = 0;

        // Variable declaration without initialization
        int secondNumber;

        secondNumber = 23;

        System.out.println("The value of the first number: " + firstNumber);
        System.out.println("The value of the second number: " + secondNumber);

        // Mini Activity

        // 1. Create a variable to contain an age value
        int age;
        age = 25;

        // 2. Create a variable to contain weight
        double weight;
        weight = 70;

        // 3. Create a variable to contain gender
        char gender ;
        gender = 'M';

        // 4. Create a variable to contain a civil status if it is single
        boolean isSingle;
        isSingle= true;

        // Create a string variable for a name
        // <data_type> <identifier>

        String name = "Nathaniel Shy";
        String concatenatedName = "Nathaniel" + " " + "Shy";

        System.out.println("Name: " + name);
        System.out.println("Concatenated Name: " + concatenatedName);
    }
}
